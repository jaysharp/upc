﻿#region Copyright
// =============================================================================================================================================
//   
//  ALL RIGHTS RESERVED. 
//
//  Unauthorized distribution, adaptation or use may be subject to civil and criminal penalties.
// ============================================================================================================================================
#endregion

#region Header
// Copyrights			: 
// Author				: Raja
// Created date			: Aug 09, 2013
// Description			:  
// History
// =========================================================================================================================
// Date			BugID		Description														Developer       Reviewed By
// =========================================================================================================================
// 08042013                Created                                                         Raja
// =========================================================================================================================
#endregion

#region Using
using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Text;
using System.Web;
using Twilight.Utils.Helpers;
using Twilight.Utils.Security;
using Twilight.Utils.Tools;
using Encoder = Twilight.Utils.Tools.Encoder;
#endregion

namespace UpcGenarator.Common
{
    #region public static class CommonLogic

    public static class CommonLogic
    {
        /// <summary>
        /// HTMLs the encode.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>string.</returns>
        public static string HtmlEncode(string value)
        {
            return Encoder.EncodeHtml(value);
        }

        /// <summary>
        /// HTMLs the decode.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>string.</returns>
        public static string HtmlDecode(string value)
        {
            return Encoder.DecodeHtml(value);
        }

        /// <summary>
        /// URLs the encode.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>string.</returns>
        [SuppressMessage("Microsoft.Design", "CA1055:UriReturnValuesShouldNotBeStrings")]
        public static string UrlEncode(string value)
        {
            return Encoder.EncodeUrl(value);
        }

        /// <summary>
        /// URLs the decode.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>string.</returns>
        [SuppressMessage("Microsoft.Design", "CA1055:UriReturnValuesShouldNotBeStrings")]
        public static string UrlDecode(string value)
        {
            return Encoder.DecodeUrl(value);
        }

        /// <summary>
        /// Strings the content of the can be dangerous.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>string.</returns>
        public static string StringCanBeDangerousContent(string value)
        {
            return Strings.CanBeDangerous(value);
        }

        /// <summary>
        /// Forms the content of the can be dangerous.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>string.</returns>
        public static string FormCanBeDangerousContent(string name)
        {
            return Forms.CanBeDangerous(name);
        }

        /// <summary>
        /// Forms the can be dangerous content mail MGR.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>string.</returns>
        public static string FormCanBeDangerousContentMailMgr(string name)
        {
            return Strings.CanBeDangerous(name);
        }

        /// <summary>
        /// Parameters the content of the can be dangerous.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>string.</returns>
        public static string ParameterCanBeDangerousContent(string name)
        {
            return Params.CanBeDangerous(name);
        }

        /// <summary>
        /// Queries the content of the string can be dangerous.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>string.</returns>
        public static string QueryStringCanBeDangerousContent(string name)
        {
            return QueryStrings.CanBeDangerous(name);
        }

        /// <summary>
        /// Cookies the content of the can be dangerous.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="decode">if set to <c>true</c> [decode].</param>
        /// <returns>string.</returns>
        public static string CookieCanBeDangerousContent(string name, bool decode)
        {
            return Cookies.CanBeDangerous(name, decode);
        }

        /// <summary>
        /// Generates the random code.
        /// </summary>
        /// <param name="numberOfDigits">The number of digits.</param>
        /// <returns>string.</returns>
        public static string GenerateRandomCode(int numberOfDigits)
        {
            return Randoms.GenerateCode(numberOfDigits);
        }

        /// <summary>
        /// Quotes the string.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns>string.</returns>
        public static string QuoteString(string data)
        {
            return StringsAndStringArrays.GetSingleQuotedString(data);
        }

        /// <summary>
        /// Password generator
        /// </summary>
        /// <param name="numberOfDigits"></param>
        /// <returns>string.</returns>
        public static string PasswordGenerate(int numberOfDigits)
        {
            return PasswordGenerator.Generate(numberOfDigits);
        }

        /// <summary>
        /// Encrypt using DES
        /// </summary>
        /// <param name="source">The data.</param>
        /// <param name="key">Encryption Key</param>
        /// <returns>Encrypted string.</returns>
        public static string EncryptionDesCrypto(string source, string key)
        {
            return Encrypter.EncryptStringByDesCrypto(source, key);
        }

        /// <summary>
        /// Decrypt using DES
        /// </summary>
        /// <param name="source">The data.</param>
        /// <param name="key">Encryption Key.</param>
        /// <returns>Decrypted string.</returns>
        public static string DecryptionDesCrypto(string source, string key)
        {
            return Encrypter.DecryptStringByDesCrypto(source, key);
        }
    }

    #endregion

    #region  public static class UrlEncoder

    public static class UrlEncoder
    {
        public static string UrlDecode(string decode)
        {
            if (decode == null) return null;
            return decode.StartsWith("=", true, CultureInfo.CurrentCulture)
                       ? FromBase64(decode.TrimStart('='))
                       : HttpUtility.UrlDecode(decode);
        }

        public static string UrlEncode(string encode)
        {
            if (encode == null) return null;
            string encoded = HttpUtility.UrlPathEncode(encode);
            return encoded.Replace("%20", "") == encode.Replace(" ", "") ? encoded : "=" + ToBase64(encode);
        }

        public static string ToBase64(string encode)
        {
            if (string.IsNullOrEmpty(encode))
                return string.Empty;

            var encoding = new UTF8Encoding();
            byte[] btByteArray = encoding.GetBytes(encode);
            string sResult = Convert.ToBase64String(btByteArray, 0, btByteArray.Length);
            sResult = sResult.Replace("+", "-").Replace("/", "_");
            return sResult;
        }

        public static string FromBase64(string decode)
        {
            if (string.IsNullOrEmpty(decode))
                return string.Empty;

            decode = decode.Replace("-", "+").Replace("_", "/");
            var encoding = new UTF8Encoding();
            return encoding.GetString(Convert.FromBase64String(decode));
        }

        public static string QueryStringEncode(string encode)
        {
            return ToBase64(encode);
        }

        public static string QueryStringDecode(string decode)
        {
            return FromBase64(decode);
        }
    }

    #endregion
}