﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using UpcGenarator.BusinessObjects;

namespace UpcGenarator.Web.Models
{
    public class UpcGenarateModel:UpcGenarate
    {
        #region Properties

        public override int Id { get; set; }

        public override int StatusId { get; set; }

        public override string ItemNumber { get; set; }

        public override string Description { get; set; }

        public override string SingleUpc { get; set; }

        public override string MasterUpc { get; set; }
        public override string InnerUpc { get; set; }

        public override string Qty { get; set; }

        public override string Innerqty { get; set; }

        public   string StatusName { get; set; }

        public override int CreatedBy { get; set; }

        public override DateTime CreateDate { get; set; }
        public SelectList Statuslist { get; set; }

        public List<UpcGenarateModel> UpcList { get; set; }
        #endregion
    }
}