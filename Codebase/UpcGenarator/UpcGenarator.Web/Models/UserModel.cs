﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using UpcGenarator.BusinessObjects;

namespace UpcGenarator.Web.Models
{
    public class UserModel:User
    {
        #region Properties

        [DisplayName(@"UserName")]
        [Required(ErrorMessage = @"UserName is required")]
        public override string UserName { get; set; }
        [DisplayName(@"Password")]
        [Required(ErrorMessage = @"Password is required")]
        public override string Password { get; set; }
        public override Boolean RememberMe { get; set; }

        #endregion
    }
}