﻿#region Copyright
// =============================================================================================================================================
//   
//  ALL RIGHTS RESERVED. 
//
//  Unauthorized distribution, adaptation or use may be subject to civil and criminal penalties.
// ============================================================================================================================================
#endregion

#region Header
// Copyrights			: 
// Author				: Raja
// Created date			: Aug 09, 2013
// Description			:  
// History
// =========================================================================================================================
// Date			BugID		Description														Developer       Reviewed By
// =========================================================================================================================
// 08042013                Created                                                         Raja
// =========================================================================================================================
#endregion

#region using

using System;
using System.Globalization;
using System.Web;
using System.Web.Mvc;
//using Bravery.Business;
//using Bravery.Businessobject.Enums;

#endregion

namespace UpcGenarator.Web.Helpers
{
    /// <summary>
    /// Helpes to handle error / exception
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public sealed class HandleErrorsAttribute : HandleErrorAttribute
    {
        #region public override void OnException(ExceptionContext filterContext)

        /// <summary>
        /// Called when an exception occurs.
        /// </summary>
        /// <param name="filterContext">The action-filter context.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="filterContext"/> parameter is null.</exception>
        public override void OnException(ExceptionContext filterContext)
        {
            if (filterContext == null) return;
            if (filterContext.HttpContext != null)
            {
                HttpContextBase ctx = filterContext.HttpContext;
                if (filterContext.Exception != null)
                {
                    if (!filterContext.ExceptionHandled && ctx.IsCustomErrorEnabled)
                    {
                        string controller = filterContext.RouteData.Values["controller"].ToString();
                        string action = filterContext.RouteData.Values["action"].ToString();

                        //ActivityLogger.Log(
                        //    string.Format(CultureInfo.CurrentCulture, "{0}Controller.{1}", controller, action),
                        //    LogType.Error,
                        //    filterContext.Exception.Message,
                        //    filterContext.Exception
                        //    );

                        var model = new HandleErrorInfo(
                            filterContext.Exception,
                            controller,
                            action
                            );

                        var result = new ViewResult
                                         {
                                             ViewName = View,
                                             MasterName = Master,
                                             ViewData = new ViewDataDictionary<HandleErrorInfo>(model),
                                             TempData = filterContext.Controller.TempData
                                         };

                        filterContext.Result = result;
                        filterContext.ExceptionHandled = true;
                        ctx.Response.Clear();
                        ctx.Response.StatusCode = 500;
                        ctx.Response.TrySkipIisCustomErrors = true;
                        return;
                    }
                }
            }
            base.OnException(filterContext);
        }

        #endregion
    }
}