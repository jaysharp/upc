﻿#region Copyright

// =============================================================================================================================================
//  Copyright (c) 2012 The District in Bearden
//  ALL RIGHTS RESERVED.
//  PO Box 10443
//  Knoxville, TN 37939
//
//  Unauthorized distribution, adaptation or use may be subject to civil and criminal penalties.
// =============================================================================================================================================


#endregion

#region Header

// Author				: Jayamurthy.Ja
// Created date			: Nov 14, 2011
// Description			: Ajax Script Helper - it contains the heper methods to generate script for ajax
// History
// =========================================================================================================================
// Date			BugID		Description														Developer       Reviewed By
// =========================================================================================================================
//
// =========================================================================================================================

#endregion

#region NameSpace

using System;
using System.ComponentModel;
using System.Globalization;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

#endregion

namespace UpcGenarator.Web.Helpers
{
    internal static class AjaxScriptHelper
    {
        #region internal static string GetOnClickScript(AjaxHelper helper, string action, string controller, object routeValues, AjaxOptions ajaxOptions)

        /// <summary>
        /// Gets the on click script.
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="action">The action.</param>
        /// <param name="controller">The controller.</param>
        /// <param name="routeValues">The route values.</param>
        /// <param name="ajaxOptions">The ajax options.</param>
        /// <returns></returns>
        internal static string GetOnClickScript(AjaxHelper helper, string action, string controller, object routeValues,
                                                AjaxOptions ajaxOptions)
        {
            UrlHelper urlHelper = GetUrlHelper(helper);

            string url = String.IsNullOrEmpty(ajaxOptions.Url) ? urlHelper.Action(action, controller) : ajaxOptions.Url;

            var builder = new StringBuilder();

            if (!String.IsNullOrEmpty(ajaxOptions.Confirm))
            {
                builder.Append("if ( confirm('" + ajaxOptions.Confirm.Replace("'", "\'") + "') ) {");
            }

            string method = String.Empty;

            if (String.IsNullOrEmpty(ajaxOptions.HttpMethod) ||
                ajaxOptions.HttpMethod.ToLower(CultureInfo.CurrentCulture) == "get")
                method = "GET";
            else if (ajaxOptions.HttpMethod.ToLower(CultureInfo.CurrentCulture) == "post")
                method = "POST";

            builder.Append("$.ajax({ type: '" + method + "'");

            builder.Append(", url: '" + url + "'");

            if (routeValues != null)
                builder.Append(", data: '" + GetRouteValuesString(urlHelper, routeValues) + "'");

            builder.Append(", success: " + GetSuccessScript(ajaxOptions));

            if (!String.IsNullOrEmpty(ajaxOptions.OnBegin))
                builder.Append(", beforeSend: " + ajaxOptions.OnBegin);

            if (!String.IsNullOrEmpty(ajaxOptions.OnFailure))
                builder.Append(", error: function(requestObject) { " + ajaxOptions.OnFailure +
                               "(requestObject.responseText); }");

            if (!String.IsNullOrEmpty(ajaxOptions.OnComplete))
                builder.Append(", complete: " + ajaxOptions.OnComplete);

            builder.Append("});");

            if (!String.IsNullOrEmpty(ajaxOptions.Confirm))
            {
                builder.Append("}");
            }

            return builder.ToString();
        }

        #endregion

        #region internal static string GetSuccessScript(AjaxOptions ajaxOptions)

        /// <summary>
        /// Gets the success script.
        /// </summary>
        /// <param name="ajaxOptions">The ajax options.</param>
        /// <returns></returns>
        internal static string GetSuccessScript(AjaxOptions ajaxOptions)
        {
            var script = new StringBuilder();

            script.Append("function(data) { ");

            if (!String.IsNullOrEmpty(ajaxOptions.UpdateTargetId))
            {
                script.Append("$('#" + ajaxOptions.UpdateTargetId + "').");

                if (ajaxOptions.InsertionMode == InsertionMode.InsertAfter)
                    script.Append("append(");
                else if (ajaxOptions.InsertionMode == InsertionMode.InsertBefore)
                    script.Append("prepend(");
                else
                    script.Append("html(");

                script.Append("data);");
            }

            if (!String.IsNullOrEmpty(ajaxOptions.OnSuccess))
                script.Append(" " + ajaxOptions.OnSuccess + ";");

            script.Append("}");

            return script.ToString();
        }

        #endregion

        #region internal static UrlHelper GetUrlHelper(AjaxHelper helper)

        /// <summary>
        /// Gets the URL helper.
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <returns></returns>
        internal static UrlHelper GetUrlHelper(AjaxHelper helper)
        {
            return helper.ViewDataContainer is ViewUserControl
                       ? ((ViewUserControl) helper.ViewDataContainer).Url
                       : (helper.ViewDataContainer is ViewPage
                              ? ((ViewPage) helper.ViewDataContainer).Url
                              : new UrlHelper(helper.ViewContext.RequestContext));
        }

        #endregion

        #region internal static string GetRouteValuesString(UrlHelper urlHelper, object routeValues)

        /// <summary>
        /// Gets the route values string.
        /// </summary>
        /// <param name="urlHelper">The URL helper.</param>
        /// <param name="routeValues">The route values.</param>
        /// <returns></returns>
        internal static string GetRouteValuesString(UrlHelper urlHelper, object routeValues)
        {
            var routeValueString = new StringBuilder();

            int paramCount = 0;

            foreach (PropertyDescriptor propertyDescriptor in TypeDescriptor.GetProperties(routeValues))
            {
                string name = propertyDescriptor.Name;
                object value = propertyDescriptor.GetValue(routeValues);

                if (value != null)
                {
                    if (paramCount > 0)
                        routeValueString.Append("&");
                    if (value.ToString().Contains("$") || value.ToString().Contains("#"))
                    {
                        routeValueString.AppendFormat("{0}=' + {1} + '", name, value);
                    }
                    else
                    {
                        routeValueString.Append(name + "=" + urlHelper.Encode(value.ToString()));
                    }
                }

                paramCount++;
            }

            return routeValueString.ToString();
        }

        #endregion

        //#region internal static string InsertionModeToString(InsertionMode insertionMode)

        ///// <summary>
        ///// Insertions the mode to string.
        ///// </summary>
        ///// <param name="insertionMode">The insertion mode.</param>
        ///// <returns></returns>
        //internal static string InsertionModeToString(InsertionMode insertionMode)
        //{
        //    switch (insertionMode)
        //    {
        //        case InsertionMode.Replace:
        //            return "Sys.Mvc.InsertionMode.replace";

        //        case InsertionMode.InsertBefore:
        //            return "Sys.Mvc.InsertionMode.insertBefore";

        //        case InsertionMode.InsertAfter:
        //            return "Sys.Mvc.InsertionMode.insertAfter";
        //    }
        //    var num = (int) insertionMode;
        //    return num.ToString(CultureInfo.InvariantCulture);
        //}

        //#endregion
    }
}