﻿#region Copyright
// =============================================================================================================================================
//   
//  ALL RIGHTS RESERVED. 
//
//  Unauthorized distribution, adaptation or use may be subject to civil and criminal penalties.
// ============================================================================================================================================
#endregion

#region Header
// Copyrights			: 
// Author				: Raja
// Created date			: Aug 09, 2013
// Description			:  
// History
// =========================================================================================================================
// Date			BugID		Description														Developer       Reviewed By
// =========================================================================================================================
// 08042013                Created                                                         Raja
// =========================================================================================================================
#endregion


#region using

using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

#endregion

namespace UpcGenarator.Web.Helpers
{
    #region public static class ExtensionMethods

    /// <summary>
    /// Extension Methods.
    /// </summary>
    public static class ExtensionMethods
    {
        #region String Extension Methods

        /// <summary>
        /// Returns part of a string up to the specified number of characters, while maintaining full words
        /// </summary>
        /// <param name="s"></param>
        /// <param name="length">Maximum characters to be returned</param>
        /// <returns>String</returns>
        public static string Chop(this string s, int length)
        {
            if (string.IsNullOrEmpty(s))
                return string.Empty;

            if (s.Length <= length)
                return s;

            string[] words = s.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            var sb = new StringBuilder();

            if (words.Length > 1)
            {
                foreach (string word in words)
                {
                    if (word.Length > length && string.IsNullOrEmpty(sb.ToString()))
                        sb.Append(word.Substring(0, length));

                    if (word.Length + sb.ToString().Length <= length)
                        sb.Append(word + " ");
                }
            }
            else
            {
                sb.Append(s.Substring(0, length));
            }

            string final = sb.ToString().TrimEnd(' ');
            return final + "...";
        }

        /// <summary>
        /// Breaking word by size with empty spaces.
        /// </summary>
        /// <param name="text">passing text</param>
        /// <param name="size">passing size</param>
        /// <returns>return values</returns>
        public static string BreakWord(this string text, int size)
        {
            return BreakWord(text, size, " ");
        }

        /// <summary>
        /// Description for BreakText
        /// </summary>
        /// <param name="text">passing text</param>
        /// <param name="size">passing size</param>
        /// <returns>return values</returns>
        public static string BreakText(this string text, int size)
        {
            return BreakWord(text, size);
        }

        /// <summary>
        /// Breaking word based on size.
        /// </summary>
        /// <param name="text">passing text</param>
        /// <param name="size">passing size</param>
        /// <param name="breaker">passing breaker</param>
        /// <returns>return values</returns>
        public static string BreakWord(this string text, int size, string breaker)
        {
            if (string.IsNullOrEmpty(text)) throw new ArgumentException("text");
            if (string.IsNullOrEmpty(breaker)) throw new ArgumentException("breaker");

            string[] split = text.Split(' ');
            string s = string.Empty;
            foreach (string words in split)
            {
                string finalWord = words;
                if (words.Length > size)
                {
                    int offset = words.Length/size;
                    finalWord = string.Empty;
                    int lastIndex = 1;
                    for (int count = 0; count < offset; count++)
                    {
                        int index = (count*size);
                        finalWord += words.Substring(index, size) + breaker; //words.Insert(index, breaker);
                        lastIndex = index + size;
                    }

                    int modValue = words.Length%size;
                    if (modValue > 0)
                    {
                        finalWord += words.Substring(lastIndex, modValue);
                    }
                }

                s += finalWord + " ";
            }

            return s.Trim();
        }

        /// <summary>
        /// Description for BreakText
        /// </summary>
        /// <param name="text">passing text</param>
        /// <param name="size">passing size</param>
        /// <param name="breaker">passing breaker</param>
        /// <returns>return values</returns>
        public static string BreakText(this string text, int size, string breaker)
        {
            string finalWord = string.Empty;

            if (string.IsNullOrEmpty(text)) return string.Empty;

            if (text.Length <= size || size <= 0) return text;

            if (text.Length > size)
            {
                int offset = text.Length/size;
                finalWord = string.Empty;
                int lastIndex = 1;
                for (int count = 0; count < offset; count++)
                {
                    int index = (count*size);
                    finalWord += text.Substring(index, size) + breaker; //words.Insert(index, breaker);
                    lastIndex = index + size;
                }

                int modValue = text.Length%size;

                if (modValue > 0)
                    finalWord += text.Substring(lastIndex, modValue);
            }

            return finalWord.Trim();
        }

        /// <summary>
        /// Breaking work by HTML Line.
        /// </summary>
        /// <param name="text">passing text</param>
        /// <param name="size">passing size</param>
        /// <returns>return values</returns>
        public static string BreakWordByLine(this string text, int size)
        {
            return BreakWord(text, size, "<br/>");
        }

        /// <summary>
        /// Description for BreakText by HTML Line.
        /// </summary>
        /// <param name="text">passing text</param>
        /// <param name="size">passing size</param>
        /// <returns>return values</returns>
        public static string BreakTextByLine(this string text, int size)
        {
            return BreakWordByLine(text, size);
        }

        /// <summary>
        /// Breaking word by Word breaker.
        /// </summary>
        /// <param name="text">passing text</param>
        /// <param name="size">passing size</param>
        /// <returns>return values</returns>
        public static string BreakWordByWbr(this string text, int size)
        {
            return BreakWord(text, size, "<wbr>");
        }

        /// <summary>
        /// Description for BreakText by Word breaker.
        /// </summary>
        /// <param name="text">passing text</param>
        /// <param name="size">passing size</param>
        /// <returns>return values</returns>
        public static string BreakTextByWbr(this string text, int size)
        {
            return BreakWordByWbr(text, size);
        }

        /// <summary>
        /// Finds web and email addresses in a string and surrounds then with the appropriate HTML anchor tags 
        /// </summary>
        /// <param name="s"></param>
        /// <returns>String</returns>
        public static string WithActiveLinks(this string s)
        {
            //Finds URLs with no protocol
            var urlregex = new Regex(@"\b\({0,1}(?<url>(www|ftp)\.[^ ,""\s<)]*)\b",
                                     RegexOptions.IgnoreCase | RegexOptions.Compiled);

            //Finds URLs with a protocol
            var httpurlregex = new Regex(@"\b\({0,1}(?<url>[^>](http://www\.|http://|https://|ftp://)[^,""\s<)]*)\b",
                                         RegexOptions.IgnoreCase | RegexOptions.Compiled);

            //Finds email addresses
            var emailregex = new Regex(@"\b(?<mail>[a-zA-Z_0-9.-]+\@[a-zA-Z_0-9.-]+\.\w+)\b",
                                       RegexOptions.IgnoreCase | RegexOptions.Compiled);

            s = urlregex.Replace(s, " <a href=\"http://${url}\" target=\"_blank\">${url}</a>");
            s = httpurlregex.Replace(s, " <a href=\"${url}\" target=\"_blank\">${url}</a>");
            s = emailregex.Replace(s, "<a href=\"mailto:${mail}\">${mail}</a>");

            return s;
        }

        /// <summary>
        /// Wraps matched strings in HTML span elements styled with a background-color
        /// </summary>
        /// <param name="text"></param>
        /// <param name="keywords">Comma-separated list of strings to be highlighted</param>
        /// <param name="cssClass">The Css color to apply</param>
        /// <param name="fullMatch">false for returning all matches, true for whole word matches only</param>
        /// <returns>string</returns>
        public static string HighlightKeyWords(this string text, string keywords, string cssClass, bool fullMatch)
        {
            if (string.IsNullOrEmpty(text) || string.IsNullOrEmpty(keywords) || string.IsNullOrEmpty(cssClass))
                return text;

            string[] words = keywords.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries);

            if (!fullMatch)
                return words.Select(word => word.Trim()).Aggregate(text,
                                                                   (current, pattern) =>
                                                                   Regex.Replace(current,
                                                                                 pattern,
                                                                                 string.Format(CultureInfo.CurrentCulture,
                                                                                     "<span style=\"background-color:{0}\">{1}</span>",
                                                                                     cssClass,
                                                                                     "$0"),
                                                                                 RegexOptions.IgnoreCase));

            return words.Select(word => "\\b" + word.Trim() + "\\b")
                .Aggregate(text, (current, pattern) =>
                                 Regex.Replace(current,
                                               pattern,
                                               string.Format(CultureInfo.CurrentCulture,"<span style=\"background-color:{0}\">{1}</span>",
                                                             cssClass,
                                                             "$0"),
                                               RegexOptions.IgnoreCase));
        }

        /// <summary>
        /// Is Integer.
        /// </summary>
        /// <param name="value">Input Value.</param>
        /// <returns>Retursn true if its valid otherwise false.</returns>
        public static bool IsInteger(this string value)
        {
            int outputValue;
            bool result = int.TryParse(value, out outputValue);
            return result;
        }

        /// <summary>
        /// Is Long.
        /// </summary>
        /// <param name="value">Input value.</param>
        /// <returns>Retursn true if its valid otherwise false.</returns>
        public static bool IsLong(this string value)
        {
            long outputValue;
            bool result = long.TryParse(value, out outputValue);
            return result;
        }

        /// <summary>
        /// Convert string into Int32.
        /// </summary>
        /// <param name="value">Input Value.</param>
        /// <returns>returns converted value.</returns>
        public static int ToInt32(this string value)
        {
            int outputValue;
            int.TryParse(value, out outputValue);
            return outputValue;
        }

        /// <summary>
        /// Convert string into Int64.  
        /// </summary>
        /// <param name="value">Input value.</param>
        /// <returns>Returns coverted Int64 value.</returns>
        public static Int64 ToInt64(this string value)
        {
            long outputValue;
            Int64.TryParse(value, out outputValue);
            return outputValue;
        }

        public static string DecodeText(this string value)
        {
            return string.IsNullOrEmpty(value) ? string.Empty : HttpContext.Current.Server.HtmlDecode(value);
        }

        public static string EncodeText(this string value)
        {
            return string.IsNullOrEmpty(value) ? string.Empty : HttpContext.Current.Server.HtmlEncode(value);
        }

        public static string ChopString(this string s, int length)
        {
            if (string.IsNullOrEmpty(s))
                return string.Empty;

            if (s.Length <= length)
                return s;

            var sb = new StringBuilder();
            sb.Append(s.Substring(0, length));

            string final = sb.ToString().TrimEnd(' ');
            return final + "...";
        }

        #endregion

        /// <summary>
        /// Clear the Cache
        /// </summary>   
        public static void ClearCache()
        {
            HttpContext.Current.Response.CacheControl = "no-cache";
            HttpContext.Current.Response.AddHeader("Pragma", "no-cache");
            HttpContext.Current.Response.Expires = -1500;
            HttpContext.Current.Response.Cache.SetExpires(DateTime.Now.AddMinutes(-1));
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Current.Response.Cache.SetNoStore();
            HttpContext.Current.Response.Cache.SetAllowResponseInBrowserHistory(false);
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        }
    }

    #endregion
}