﻿#region Copyright
// =============================================================================================================================================
//   
//  ALL RIGHTS RESERVED. 
//
//  Unauthorized distribution, adaptation or use may be subject to civil and criminal penalties.
// ============================================================================================================================================
#endregion

#region Header
// Copyrights			: 
// Author				: Raja
// Created date			: Aug 09, 2013
// Description			:  
// History
// =========================================================================================================================
// Date			BugID		Description														Developer       Reviewed By
// =========================================================================================================================
// 08042013                Created                                                         Raja
// =========================================================================================================================
#endregion


#region using

using System;
using System.Web;
using UpcGenarator.Web.Helpers;

#endregion

namespace UpcGenarator.Web.Helpers
{
    /// <summary>
    /// Cookie Helper for Web Application
    /// </summary>
    public class CookieHelper
    {
        #region Public Methods

        #region public void SetCookie(string key, string value)

        /// <summary>
        /// Sets the cookie.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public void SetCookie(string key, string value)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[key] ?? new HttpCookie(key);

            DateTime dtExpiry = DateTime.Now.AddDays(7);
            cookie.Expires = dtExpiry;
            cookie.Value = value;
            HttpContext.Current.Response.SetCookie(cookie);
        }

        #endregion

        #region public string GetCookie(string key)

        /// <summary>
        /// Gets the cookie.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public string GetCookie(string key)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[key];
            if (cookie == null)
                return string.Empty;
            try
            {
                return CommonHelper.Decryption(cookie.Value);
            }
            catch (FormatException)
            {
                return string.Empty;
            }
        }

        #endregion

        #endregion
    }
}