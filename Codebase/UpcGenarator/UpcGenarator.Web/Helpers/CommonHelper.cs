﻿#region Copyright
// =============================================================================================================================================
//   
//  ALL RIGHTS RESERVED. 
//
//  Unauthorized distribution, adaptation or use may be subject to civil and criminal penalties.
// ============================================================================================================================================
#endregion

#region Header
// Copyrights			: 
// Author				: Raja
// Created date			: Aug 09, 2013
// Description			:  
// History
// =========================================================================================================================
// Date			BugID		Description														Developer       Reviewed By
// =========================================================================================================================
// 08042013                Created                                                         Raja
// =========================================================================================================================
#endregion

#region Using 

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using UpcGenarator.Common;
using UpcGenarator.Web;

#endregion

namespace UpcGenarator.Web.Helpers
{
    #region public static class CommonHelper
    /// <summary>
    /// Common Helper for Web Application
    /// </summary>
    public static class CommonHelper
    {
        #region Declaration

        /// <summary>
        /// Key to pull the success message
        /// </summary>
        public const string SuccessMessageKey = "_successMessage";

        /// <summary>
        /// Key to pull the error message
        /// </summary>
        public const string ErrorMessageKey = "_errorMessage";

        /// <summary>
        /// Key to flag if the message can be cleared
        /// </summary>
        public const string CanClearMessageKey = "_canClearMessage";

        #endregion

        #region Public Static Methods

        #region public static string GetSuccessMessage(object data)

        /// <summary>
        /// Returns the string representation of the success message object
        /// </summary>
        /// <param name="data">Success Message object</param>
        /// <returns>string</returns>
        public static string GetSuccessMessage(object data)
        {
            return data == null ? string.Empty : data.ToString();
        }

        #endregion

        #region public static string GetErrorMessage(object data)

        /// <summary>
        /// Returns the string representation of the error message object
        /// </summary>
        /// <param name="data">Error Message object</param>
        /// <returns>string</returns>
        public static string GetErrorMessage(object data)
        {
            return data == null ? string.Empty : data.ToString();
        }

        #endregion

        #region public static string Encryption(string value)

        /// <summary>
        /// Encrypts the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Encrypted string</returns>
        public static string Encryption(string value)
        {
            return CommonLogic.EncryptionDesCrypto(value, MvcApplication.EncryptionKey);
        }

        #endregion

        #region public static string Decryption(string value)

        /// <summary>
        /// Decrypt the string value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Decrypted string</returns>
        public static string Decryption(string value)
        {
            return CommonLogic.DecryptionDesCrypto(value, MvcApplication.EncryptionKey);
       }

        #endregion

        #region public static void SetCookieInfo(bool loggedIn, string email, string password)

        /// <summary>
        /// Set Cookie Information
        /// </summary>
        /// <param name="loggedIn">Logged In or not.</param>
        /// <param name="email">Email Id.</param>
        /// <param name="password">Password.</param>
        public static void SetCookieInfo(bool loggedIn, string email, string password)
        {
            var obj = new CookieHelper();

            if (loggedIn)
            {
                obj.SetCookie("UGEmail", CommonHelper.Encryption(email));
                obj.SetCookie("UGPassword", CommonHelper.Encryption(password));
            }
            else
            {
                obj.SetCookie("UGEmail", string.Empty);
                obj.SetCookie("UGPassword", string.Empty);
            }
            obj.SetCookie("UGLoggedIn", loggedIn ? "Y" : "N");
        }

        #endregion

        #region public static void GetCookieInfo(ref bool loggedIn, ref string email, ref string password)

        /// <summary>
        /// Get LogOn Cookie information.
        /// </summary>
        /// <param name="loggedIn">Is Keep Me.</param>
        /// <param name="email">Email.</param>
        /// <param name="password">Password</param>
        public static void GetCookieInfo(ref bool loggedIn, ref string email, ref string password)
        {
            var obj = new CookieHelper();
            loggedIn = false;

            if (!string.IsNullOrEmpty(obj.GetCookie("GGLoggedIn")) &&
                !string.IsNullOrEmpty(obj.GetCookie("GGEmail")) &&
                !string.IsNullOrEmpty(obj.GetCookie("GGPassword")) &&
                string.Compare(obj.GetCookie("GGLoggedIn"), "Y", true, CultureInfo.CurrentCulture) == 0)
            {
                loggedIn = true;
                email = obj.GetCookie("GGEmail");
                password = obj.GetCookie("GGPassword");
            }
        }

        #endregion        

        #region Get Milliseconds
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static long GetCurrentMiliseconds()
        {
            return DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        }
        #endregion

        #endregion
    }

    #endregion
}