﻿#region Copyright
// =============================================================================================================================================
//  Copyright (c) 2012 The District in Bearden
//  ALL RIGHTS RESERVED.
//  PO Box 10443
//  Knoxville, TN 37939
//
//  Unauthorized distribution, adaptation or use may be subject to civil and criminal penalties.
// =============================================================================================================================================


#endregion

#region Header

// Author				: Jayamurthy.Ja
// Created date			: Nov 14, 2011
// Description			: @Ajax Extention class - it contains the extension methods of Ajax helper
// History
// =========================================================================================================================
// Date			BugID		Description														Developer       Reviewed By
// =========================================================================================================================
//
// =========================================================================================================================

#endregion

#region NameSpace

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

#endregion

namespace UpcGenarator.Web.Helpers
{ 
    #region public static class AjaxExtensions

    /// <summary>
    /// Class contains the extension methods of Ajax helper
    /// </summary>
    public static class AjaxExtensions
    {
        #region #region extension methods

        #region public static MvcHtmlString ActionButton(this AjaxHelper helper, string value, string action, string controller, AjaxOptions ajaxOptions)

        /// <summary>
        /// Actions the button.
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="value">The value.</param>
        /// <param name="action">The action.</param>
        /// <param name="controller">The controller.</param>
        /// <param name="ajaxOptions">The ajax options.</param>
        /// <returns></returns>
        public static MvcHtmlString ActionButton(this AjaxHelper helper, string value, string action, string controller,
                                                 AjaxOptions ajaxOptions)
        {
            return ActionButton(helper, value, action, controller, null, ajaxOptions, null, string.Empty, string.Empty);
        }

        #endregion

        #region public static MvcHtmlString ActionButton(this AjaxHelper helper, string value, string action, string controller, object routeValues, AjaxOptions ajaxOptions)

        /// <summary>
        /// Actions the button.
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="value">The value.</param>
        /// <param name="action">The action.</param>
        /// <param name="controller">The controller.</param>
        /// <param name="routeValues">The route values.</param>
        /// <param name="ajaxOptions">The ajax options.</param>
        /// <returns></returns>
        public static MvcHtmlString ActionButton(this AjaxHelper helper, string value, string action, string controller,
                                                 object routeValues, AjaxOptions ajaxOptions)
        {
            return ActionButton(helper, value, action, controller, routeValues, ajaxOptions, null, string.Empty,
                                string.Empty);
        }

        #endregion

        #region public static MvcHtmlString ActionButton(this AjaxHelper helper, string value, string action, string controller, object routeValues, AjaxOptions ajaxOptions, object htmlAttributes)

        /// <summary>
        /// Actions the button.
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="value">The value.</param>
        /// <param name="action">The action.</param>
        /// <param name="controller">The controller.</param>
        /// <param name="routeValues">The route values.</param>
        /// <param name="ajaxOptions">The ajax options.</param>
        /// <param name="htmlAttributes">The HTML attributes.</param>
        /// <param name="okButton">The ok button.</param>
        /// <param name="cancelButton">The cancel button.</param>
        /// <returns></returns>
        public static MvcHtmlString ActionButton(this AjaxHelper helper, string value, string action, string controller,
                                                 object routeValues, AjaxOptions ajaxOptions, object htmlAttributes,
                                                 string okButton, string cancelButton)
        {
            var input = new TagBuilder("input");
            input.Attributes.Add("type", "button");
            input.Attributes.Add("value", value);
            IEnumerable<KeyValuePair<string, object>> attrib = htmlAttributes.ToDictionary();
            var onclick = new KeyValuePair<string, object>();
            if (attrib != null)
            {
                foreach (var o in attrib)
                {
                    if (o.Key.ToLower(CultureInfo.CurrentCulture).Equals("onclick"))
                    {
                        onclick = o;
                    }
                    else
                    {
                        input.Attributes.Add(o.Key, o.Value.ToString());
                    }
                }
            }

            string onClickScript = AjaxScriptHelper.GetOnClickScript(helper, action, controller, routeValues,
                                                                     ajaxOptions);
            if (onclick.Value != null)
            {
                string onClickValue = onclick.Value.ToString(); //.ToLower(CultureInfo.CurrentCulture);
                if (onClickValue.Contains("confirm"))
                {
                    //string actualscript = onClickValue.Substring(onClickValue.IndexOf("("));
                    //actualscript = actualscript.Substring(0, actualscript.IndexOf(")") + 1);
                    //onClickScript = "if (alert" + actualscript + ") {" + onClickScript + "}";


                    string actualscript =
                        onClickValue.Substring(onClickValue.IndexOf("(", StringComparison.CurrentCultureIgnoreCase));
                    actualscript = actualscript.Substring(0,
                                                          actualscript.IndexOf(")",
                                                                               StringComparison.CurrentCultureIgnoreCase));
                    actualscript = "jConfirm" + actualscript + ", 'Confirm', function(r){if(r) {" + onClickScript +
                                   "}});";
                    onClickScript = "$.alerts.okButton='" + okButton + "';$.alerts.cancelButton='" + cancelButton +
                                    "'; " + actualscript;
                }
            }
            input.Attributes.Add("onclick", onClickScript);

            return new MvcHtmlString(input.ToString(TagRenderMode.SelfClosing));
        }

        #endregion

        #region public static MvcHtmlString ActionButton(this AjaxHelper helper, string value, string action, string controller, object routeValues, AjaxOptions ajaxOptions, object htmlAttributes)

        /// <summary>
        /// Actions the button.
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="value">The value.</param>
        /// <param name="action">The action.</param>
        /// <param name="controller">The controller.</param>
        /// <param name="routeValues">The route values.</param>
        /// <param name="ajaxOptions">The ajax options.</param>
        /// <param name="htmlAttributes">The HTML attributes.</param>
        /// <returns></returns>
        public static MvcHtmlString ActionButton(this AjaxHelper helper, string value, string action, string controller,
                                                 object routeValues, AjaxOptions ajaxOptions, object htmlAttributes)
        {
            return ActionButton(helper, value, action, controller, routeValues, ajaxOptions, htmlAttributes,
                                string.Empty, string.Empty);
        }

        #endregion

        #region private static IEnumerable<KeyValuePair<string, object>> ToDictionary(this object data)

        /// <summary>
        /// Toes the dictionary.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        private static IEnumerable<KeyValuePair<string, object>> ToDictionary(this object data)
        {
            if (data == null) return null;

            const BindingFlags publicAttributes = BindingFlags.Public | BindingFlags.Instance;

            return
                data.GetType().GetProperties(publicAttributes).Where(property => property.CanRead).ToDictionary(
                    property => property.Name, property => property.GetValue(data, null));
        }

        #endregion

        #region public static MvcHtmlString DropDownList(this AjaxHelper helper, string name, string action, string controller, object routeValues, IEnumerable<SelectListItem> selectList, AjaxOptions ajaxOptions, object htmlAttributes)

        /// <summary>
        /// Drops down list.
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="name">The name.</param>
        /// <param name="action">The action.</param>
        /// <param name="controller">The controller.</param>
        /// <param name="routeValues">The route values.</param>
        /// <param name="selectList">The select list.</param>
        /// <param name="ajaxOptions">The ajax options.</param>
        /// <param name="htmlAttributes">The HTML attributes.</param>
        /// <returns></returns>
        public static MvcHtmlString DropDownList(this AjaxHelper helper, string name, string action, string controller,
                                                 object routeValues, IEnumerable<SelectListItem> selectList,
                                                 AjaxOptions ajaxOptions, object htmlAttributes)
        {
            var select = new TagBuilder("select");
            select.Attributes.Add("name", name);
            select.Attributes.Add("id", name);

            IEnumerable<KeyValuePair<string, object>> attrib = htmlAttributes.ToDictionary();
            var onchange = new KeyValuePair<string, object>();
            if (attrib != null)
            {
                foreach (var o in attrib)
                {
                    if (o.Key.ToLower(CultureInfo.CurrentCulture).Equals("onclick"))
                    {
                        onchange = o;
                    }
                    else
                    {
                        select.Attributes.Add(o.Key, o.Value.ToString());
                    }
                }
            }


            string onChangeScript = AjaxScriptHelper.GetOnClickScript(helper, action, controller, routeValues,
                                                                      ajaxOptions);
            if (onchange.Value != null)
            {
                string onChangeValue = onchange.Value.ToString().ToLower(CultureInfo.CurrentCulture);
                if (onChangeValue.Contains("confirm"))
                {
                    //string actualscript = onClickValue.Substring(onClickValue.IndexOf("("));
                    //actualscript = actualscript.Substring(0, actualscript.IndexOf(")") + 1);
                    //onClickScript = "if (alert" + actualscript + ") {" + onClickScript + "}";


                    string actualscript =
                        onChangeValue.Substring(onChangeValue.IndexOf("(", StringComparison.CurrentCultureIgnoreCase));
                    actualscript = actualscript.Substring(0,
                                                          actualscript.IndexOf(")",
                                                                               StringComparison.CurrentCultureIgnoreCase));
                    actualscript = "jConfirm" + actualscript + ", 'Confirm', function(r){if(r) {" + onChangeScript +
                                   "}});";
                    onChangeScript =
                        "$.alerts.okButton='Yes, delete this address';$.alerts.cancelButton='No, I made a mistake'; " +
                        actualscript;
                }
            }

            select.Attributes.Add("onchange", onChangeScript);

            var options = new StringBuilder();
            if (selectList != null)
            {
                foreach (SelectListItem item in selectList)
                {
                    var option = new TagBuilder("option");
                    option.Attributes.Add("value", item.Value);
                    if (item.Selected)
                    {
                        option.Attributes.Add("selected", "selected");
                    }
                    option.InnerHtml = item.Text;

                    options.AppendLine(option.ToString(TagRenderMode.Normal));
                }
            }

            select.InnerHtml = options.ToString();
            return new MvcHtmlString(select.ToString(TagRenderMode.Normal));
        }

        #endregion

        #region public static MvcHtmlString DropDownList(this AjaxHelper helper, string name, string action, string controller, IEnumerable<SelectListItem> selectList, AjaxOptions ajaxOptions)

        /// <summary>
        /// Drops down list.
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="name">The name.</param>
        /// <param name="action">The action.</param>
        /// <param name="controller">The controller.</param>
        /// <param name="selectList">The select list.</param>
        /// <param name="ajaxOptions">The ajax options.</param>
        /// <returns></returns>
        public static MvcHtmlString DropDownList(this AjaxHelper helper, string name, string action, string controller,
                                                 IEnumerable<SelectListItem> selectList, AjaxOptions ajaxOptions)
        {
            return DropDownList(helper, name, action, controller, null, selectList, ajaxOptions, null);
        }

        #endregion

        #region public static MvcHtmlString DropDownList(this AjaxHelper helper, string name, string action, string controller, object routeValues, IEnumerable<SelectListItem> selectList, AjaxOptions ajaxOptions)

        /// <summary>
        /// Drops down list.
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="name">The name.</param>
        /// <param name="action">The action.</param>
        /// <param name="controller">The controller.</param>
        /// <param name="routeValues">The route values.</param>
        /// <param name="selectList">The select list.</param>
        /// <param name="ajaxOptions">The ajax options.</param>
        /// <returns></returns>
        public static MvcHtmlString DropDownList(this AjaxHelper helper, string name, string action, string controller,
                                                 object routeValues, IEnumerable<SelectListItem> selectList,
                                                 AjaxOptions ajaxOptions)
        {
            return DropDownList(helper, name, action, controller, routeValues, selectList, ajaxOptions, null);
        }

        #endregion

        #region public static MvcHtmlString ActionButtonLink(this AjaxHelper helper, string linkText, string action, string controller, AjaxOptions ajaxOptions)

        /// <summary>
        /// Actions the button.
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="linkText">The linkText.</param>
        /// <param name="action">The action.</param>
        /// <param name="controller">The controller.</param>
        /// <param name="ajaxOptions">The ajax options.</param>
        /// <returns></returns>
        public static MvcHtmlString ActionButtonLink(this AjaxHelper helper, string linkText, string action,
                                                     string controller,
                                                     AjaxOptions ajaxOptions)
        {
            return ActionButtonLink(helper, linkText, action, controller, null, ajaxOptions, null, string.Empty,
                                    string.Empty);
        }

        #endregion

        #region public static MvcHtmlString ActionButtonLink(this AjaxHelper helper, string linkText, string action, string controller, object routeValues, AjaxOptions ajaxOptions)

        /// <summary>
        /// Actions the button.
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="linkText">The linkText.</param>
        /// <param name="action">The action.</param>
        /// <param name="controller">The controller.</param>
        /// <param name="routeValues">The route Values.</param>
        /// <param name="ajaxOptions">The ajax options.</param>
        /// <returns></returns>
        public static MvcHtmlString ActionButtonLink(this AjaxHelper helper, string linkText, string action,
                                                     string controller,
                                                     object routeValues, AjaxOptions ajaxOptions)
        {
            return ActionButtonLink(helper, linkText, action, controller, routeValues, ajaxOptions, null, string.Empty,
                                    string.Empty);
        }

        #endregion

        #region public static MvcHtmlString ActionButtonLink(this AjaxHelper helper, string linkText, string action, string controller, object routeValues, AjaxOptions ajaxOptions, object htmlAttributes)

        /// <summary>
        /// Actions the button.
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="linkText">The linkText.</param>
        /// <param name="action">The action.</param>
        /// <param name="controller">The controller.</param>
        /// <param name="routeValues">The route Values.</param>
        /// <param name="ajaxOptions">The ajax options.</param>
        /// <param name="htmlAttributes">The HTML attributes.</param>
        /// <param name="okButton">The ok button.</param>
        /// <param name="cancelButton">The cancel button.</param>
        /// <returns></returns>
        public static MvcHtmlString ActionButtonLink(this AjaxHelper helper, string linkText, string action,
                                                     string controller,
                                                     object routeValues, AjaxOptions ajaxOptions, object htmlAttributes,
                                                     string okButton, string cancelButton)
        {
            var input = new TagBuilder("a");
            input.Attributes.Add("href", "#");
            input.SetInnerText(linkText);
            IEnumerable<KeyValuePair<string, object>> attrib = htmlAttributes.ToDictionary();
            var onclick = new KeyValuePair<string, object>();
            if (attrib != null)
            {
                foreach (var o in attrib)
                {
                    if (o.Key.ToLower(CultureInfo.CurrentCulture).Equals("onclick"))
                    {
                        onclick = o;
                    }
                    else
                    {
                        input.Attributes.Add(o.Key, o.Value.ToString());
                    }
                }
            }

            string onClickScript = AjaxScriptHelper.GetOnClickScript(helper, action, controller, routeValues,
                                                                     ajaxOptions);
            if (onclick.Value != null)
            {
                string onClickValue = onclick.Value.ToString(); //.ToLower(CultureInfo.CurrentCulture);
                if (onClickValue.Contains("confirm"))
                {
                    //string actualscript = onClickValue.Substring(onClickValue.IndexOf("("));
                    //actualscript = actualscript.Substring(0, actualscript.IndexOf(")") + 1);
                    //onClickScript = "if (alert" + actualscript + ") {" + onClickScript + "}";


                    string actualscript =
                        onClickValue.Substring(onClickValue.IndexOf("(", StringComparison.CurrentCultureIgnoreCase));
                    actualscript = actualscript.Substring(0,
                                                          actualscript.IndexOf(")",
                                                                               StringComparison.CurrentCultureIgnoreCase));
                    actualscript = "jConfirm" + actualscript + ", 'Confirm', function(r){if(r) {" + onClickScript +
                                   "}});";
                    onClickScript = "$.alerts.okButton='" + okButton + "';$.alerts.cancelButton='" + cancelButton +
                                    "'; " + actualscript;
                }
            }
            input.Attributes.Add("onclick", onClickScript);

            return new MvcHtmlString(input.ToString(TagRenderMode.Normal));
        }

        #endregion

        #region public static MvcHtmlString ActionButtonLink(this AjaxHelper helper, string Value, string action, string controller, object routeValues, AjaxOptions ajaxOptions, object htmlAttributes)

        /// <summary>
        /// Actions the button.
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="linkText">The linkText.</param>
        /// <param name="action">The action.</param>
        /// <param name="controller">The controller.</param>
        /// <param name="routeValues">The route Values.</param>
        /// <param name="ajaxOptions">The ajax options.</param>
        /// <param name="htmlAttributes">The HTML attributes.</param>
        /// <returns></returns>
        public static MvcHtmlString ActionButtonLink(this AjaxHelper helper, string linkText, string action,
                                                     string controller,
                                                     object routeValues, AjaxOptions ajaxOptions, object htmlAttributes)
        {
            return ActionButtonLink(helper, linkText, action, controller, routeValues, ajaxOptions, htmlAttributes,
                                    string.Empty, string.Empty);
        }

        #endregion

        #region public static MvcHtmlString ImageActionLink(this AjaxHelper helper, string imageUrl, string altText, string actionName, object routeValues, AjaxOptions ajaxOptions, object htmlAttributes)

        /// <summary>
        /// Images the action link.
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="imageUrl">The image URL.</param>
        /// <param name="altText">The alt text.</param>
        /// <param name="actionName">Name of the action.</param>
        /// <param name="routeValues">The route values.</param>
        /// <param name="ajaxOptions">The ajax options.</param>
        /// <param name="htmlAttributes">The HTML attributes.</param>
        /// <returns></returns>
        public static MvcHtmlString ImageActionLink(this AjaxHelper helper, Uri imageUrl, string altText,
                                                    string actionName, object routeValues, AjaxOptions ajaxOptions,
                                                    object htmlAttributes)
        {
            var builder = new TagBuilder("img");
            builder.MergeAttribute("src", (imageUrl == null) ? "" : imageUrl.ToString());
            builder.MergeAttribute("alt", altText);
            string link =
                helper.ActionLink("[replaceme]", actionName, routeValues, ajaxOptions, htmlAttributes).ToHtmlString();
            return new MvcHtmlString(link.Replace("[replaceme]",
                                                  builder.ToString(TagRenderMode.SelfClosing)));
        }

        #endregion

        #endregion
    }

    #endregion
}