﻿#region Copyright
// =============================================================================================================================================
//   
//  ALL RIGHTS RESERVED. 
//
//  Unauthorized distribution, adaptation or use may be subject to civil and criminal penalties.
// ============================================================================================================================================
#endregion

#region Header
// Copyrights			: 
// Author				: Raja
// Created date			: Aug 09, 2013
// Description			:  
// History
// =========================================================================================================================
// Date			BugID		Description														Developer       Reviewed By
// =========================================================================================================================
// 08042013                Created                                                         Raja
// =========================================================================================================================
#endregion


namespace UpcGenarator.Web.Helpers
{
    public class BusinessObjectsInfo
    {
        #region Public properties

        public string Email { get; set; }

        //public int StatsTypesId { get; set; }

        public string BackUrl { get; set; }

        #endregion
    }
}