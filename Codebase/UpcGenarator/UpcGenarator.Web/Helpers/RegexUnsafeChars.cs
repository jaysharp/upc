﻿#region Copyright
// =============================================================================================================================================
//   
//  ALL RIGHTS RESERVED. 
//
//  Unauthorized distribution, adaptation or use may be subject to civil and criminal penalties.
// ============================================================================================================================================
#endregion

#region Header
// Copyrights			: 
// Author				: Raja
// Created date			: Aug 09, 2013
// Description			:  
// History
// =========================================================================================================================
// Date			BugID		Description														Developer       Reviewed By
// =========================================================================================================================
// 08042013                Created                                                         Raja
// =========================================================================================================================
#endregion

#region Using

using System;
using System.ComponentModel.DataAnnotations;

#endregion 

namespace UpcGenarator.Web.Helpers
{
    /// <summary>
    /// RegExUnsafeChar Pattern classs.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public sealed class RegexUnsafeCharsAttribute : RegularExpressionAttribute
    {
        #region Constructors

        #region public RegexUnsafeChars()

        /// <summary>
        /// Initializes a new instance of the <see cref="RegexUnsafeCharsAttribute"/> class.
        /// </summary>
        public RegexUnsafeCharsAttribute()
            : base(@"^[^\u003C\u003E]*$")
        {
            ErrorMessage = @"Invalid characters < and >";
        }

        #endregion

        #endregion
    }
}