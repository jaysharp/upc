﻿#region Copyright
// =============================================================================================================================================
//   
//  ALL RIGHTS RESERVED. 
//
//  Unauthorized distribution, adaptation or use may be subject to civil and criminal penalties.
// ============================================================================================================================================
#endregion

#region Header
// Copyrights			: 
// Author				: Raja
// Created date			: Aug 09, 2013
// Description			:  
// History
// =========================================================================================================================
// Date			BugID		Description														Developer       Reviewed By
// =========================================================================================================================
// 08042013                Created                                                         Raja
// =========================================================================================================================
#endregion


#region using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;

#endregion

namespace UpcGenarator.Web.Helpers
{
    #region public static class HtmlExtensions

    /// <summary>
    /// Class contains the extension methods of html helper
    /// </summary>
    public static class HtmlExtensions
    {
        #region extension methods

        #region public static MvcHtmlString ActionButton(this HtmlHelper helper, string value, string action, string controller)

        /// <summary>
        /// Actions the button.
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="value">The value.</param>
        /// <param name="action">The action.</param>
        /// <param name="controller">The controller.</param>
        /// <returns></returns>
        public static MvcHtmlString ActionButton(this HtmlHelper helper, string value, string action, string controller)
        {
            return ActionButton(helper, value, action, controller, null, null, string.Empty, string.Empty);
        }

        #endregion

        #region public static MvcHtmlString ActionButton(this HtmlHelper helper, string value, string action)

        /// <summary>
        /// Actions the button.
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="value">The value.</param>
        /// <param name="action">The action.</param>
        /// <returns></returns>
        public static MvcHtmlString ActionButton(this HtmlHelper helper, string value, string action)
        {
            return ActionButton(helper, value, action, null, null, null, string.Empty, string.Empty);
        }

        #endregion

        #region public static MvcHtmlString ActionButton(this HtmlHelper helper, string value, string action, string controller, object routeValues)

        /// <summary>
        /// Actions the button.
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="value">The value.</param>
        /// <param name="action">The action.</param>
        /// <param name="controller">The controller.</param>
        /// <param name="routeValues">The route values.</param>
        /// <returns></returns>
        public static MvcHtmlString ActionButton(this HtmlHelper helper, string value, string action, string controller,
                                                 object routeValues)
        {
            return ActionButton(helper, value, action, controller, routeValues, null, string.Empty, string.Empty);
        }

        #endregion

        #region public static MvcHtmlString ActionButton(this HtmlHelper helper, string value, string action, string controller, object routeValues, object htmlAttributes)

        /// <summary>
        /// Actions the button.
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="value">The value.</param>
        /// <param name="action">The action.</param>
        /// <param name="controller">The controller.</param>
        /// <param name="routeValues">The route values.</param>
        /// <param name="htmlAttributes">The HTML attributes.</param>
        /// <param name="okButton">The ok button.</param>
        /// <param name="cancelButton">The cancel button.</param>
        /// <returns></returns>
        public static MvcHtmlString ActionButton(this HtmlHelper helper, string value, string action, string controller,
                                                 object routeValues, object htmlAttributes, string okButton,
                                                 string cancelButton)
        {
            if (helper == null)
            {
                return new MvcHtmlString(string.Empty);
            }
            string url = (new UrlHelper(helper.ViewContext.RequestContext)).Action(action, controller);
            url = url + "?" + GetRouteValuesString(new UrlHelper(helper.ViewContext.RequestContext), routeValues);
            var input = new TagBuilder("input");
            input.Attributes.Add("type", "button");
            input.Attributes.Add("value", value);

            IEnumerable<KeyValuePair<string, object>> attrib = htmlAttributes.ToDictionary();
            var onclick = new KeyValuePair<string, object>();
            if (attrib != null)
            {
                foreach (var o in attrib)
                {
                    if (o.Key.ToLower(CultureInfo.CurrentCulture).Equals("onclick"))
                    {
                        onclick = o;
                    }
                    else
                    {
                        input.Attributes.Add(o.Key, o.Value.ToString());
                    }
                }
            }

            string onClickScript = "location.href='" + url + "';";
            if (onclick.Value != null)
            {
                string onClickValue = onclick.Value.ToString(); //.ToLower(CultureInfo.CurrentCulture);
                if (onClickValue.Contains("confirm"))
                {
                    string actualscript =
                        onClickValue.Substring(onClickValue.IndexOf("(", StringComparison.CurrentCultureIgnoreCase));
                    actualscript = actualscript.Substring(0,
                                                          actualscript.IndexOf(")",
                                                                               StringComparison.CurrentCultureIgnoreCase));
                    actualscript = "jConfirm" + actualscript + ", 'Confirm', function(r){if(r) {" + onClickScript +
                                   "}});";
                    onClickScript = "$.alerts.okButton='" + okButton + "';$.alerts.cancelButton='" + cancelButton +
                                    "'; " + actualscript;
                }
            }
            input.Attributes.Add("onclick", onClickScript);

            return new MvcHtmlString(input.ToString(TagRenderMode.SelfClosing));
        }

        #endregion

        #region public static MvcHtmlString ActionButton(this HtmlHelper helper, string value, string action, string controller, object routeValues, object htmlAttributes)

        /// <summary>
        /// Actions the button.
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="value">The value.</param>
        /// <param name="action">The action.</param>
        /// <param name="controller">The controller.</param>
        /// <param name="routeValues">The route values.</param>
        /// <param name="htmlAttributes">The HTML attributes.</param>
        /// <returns></returns>
        public static MvcHtmlString ActionButton(this HtmlHelper helper, string value, string action, string controller,
                                                 object routeValues, object htmlAttributes)
        {
            return ActionButton(helper, value, action, controller, routeValues, htmlAttributes, string.Empty,
                                string.Empty);
        }

        #endregion

        #region private static IEnumerable<KeyValuePair<string, object>> ToDictionary(this object data)

        /// <summary>
        /// Toes the dictionary.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        private static IEnumerable<KeyValuePair<string, object>> ToDictionary(this object data)
        {
            if (data == null) return null;

            const BindingFlags publicAttributes = BindingFlags.Public | BindingFlags.Instance;

            return
                data.GetType().GetProperties(publicAttributes).Where(property => property.CanRead).ToDictionary(
                    property => property.Name, property => property.GetValue(data, null));
        }

        #endregion

        #region public static MvcHtmlString Table(this HtmlHelper helper, string name, IEnumerable<Object> items, IDictionary<string, object> attributes)

        /// <summary>
        /// Tables the specified helper.
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="name">The name.</param>
        /// <param name="items">The items.</param>
        /// <param name="attributes">The attributes.</param>
        /// <returns></returns>
        public static MvcHtmlString Table(this HtmlHelper helper, string name, IEnumerable<Object> items,
                                          IDictionary<string, object> attributes)
        {
            if (helper == null) return new MvcHtmlString(string.Empty);
            if (items == null || string.IsNullOrEmpty(name))
            {
                return new MvcHtmlString(string.Empty);
            }
            return BuildTable(name, items, attributes);
        }

        #endregion

        #region public static MvcHtmlString CheckBoxList(this HtmlHelper helper, string name, IDictionary<string, string> items)

        public static MvcHtmlString CheckBoxList(this HtmlHelper helper, string name, IDictionary<string, string> items)
        {
            return CheckBoxList(helper, name, items, null, null);
        }

        #endregion

        #region public static MvcHtmlString CheckBoxList(this HtmlHelper helper, string name, IDictionary<string, string> items,...

        public static MvcHtmlString CheckBoxList(this HtmlHelper helper, string name, IDictionary<string, string> items,
                                                 IDictionary<string, object> checkBoxHtmlAttributes)
        {
            return CheckBoxList(helper, name, items, null, checkBoxHtmlAttributes);
        }

        #endregion

        #region public static MvcHtmlString CheckBoxList(this HtmlHelper helper, string name, IDictionary<string, string> items,...

        public static MvcHtmlString CheckBoxList(this HtmlHelper helper, string name, IDictionary<string, string> items,
                                                 IEnumerable<string> selectedValues)
        {
            return CheckBoxList(helper, name, items, selectedValues, null);
        }

        #endregion

        #region public static MvcHtmlString CheckBoxList(this HtmlHelper helper, string name, IDictionary<string, string> items,...

        public static MvcHtmlString CheckBoxList(this HtmlHelper helper, string name, IDictionary<string, string> items,
                                                 IEnumerable<string> selectedValues,
                                                 IDictionary<string, object> checkBoxHtmlAttributes)
        {
            IEnumerable<SelectListItem> selectListItems = from i in items
                                                          select new SelectListItem
                                                                     {
                                                                         Text = i.Key,
                                                                         Value = i.Value,
                                                                         Selected =
                                                                             (selectedValues != null &&
                                                                              selectedValues.Contains(i.Value))
                                                                     };
            return CheckBoxList(helper, name, selectListItems, checkBoxHtmlAttributes);
        }

        #endregion

        #region public static MvcHtmlString CheckBoxList(this HtmlHelper helper, string name, IEnumerable<SelectListItem> items)

        public static MvcHtmlString CheckBoxList(this HtmlHelper helper, string name, IEnumerable<SelectListItem> items)
        {
            return CheckBoxList(helper, name, items, null);
        }

        #endregion

        #region public static MvcHtmlString CheckBoxList(this HtmlHelper helper, string name, IEnumerable<SelectListItem> items, IDictionary<string, object> checkBoxHtmlAttributes)

        public static MvcHtmlString CheckBoxList(this HtmlHelper helper, string name, IEnumerable<SelectListItem> items,
                                                 IDictionary<string, object> checkBoxHtmlAttributes)
        {
            var output = new StringBuilder();
            if (helper == null) return new MvcHtmlString(string.Empty);
            if (items == null) return new MvcHtmlString(string.Empty);
            foreach (SelectListItem item in items)
            {
                output.Append("<div class=\"fields\"><label>");
                var checkboxList = new TagBuilder("input");
                checkboxList.MergeAttribute("type", "checkbox");
                checkboxList.MergeAttribute("name", name);
                checkboxList.MergeAttribute("value", item.Value);
                // Check to see if it’s checked
                if (item.Selected)
                    checkboxList.MergeAttribute("checked", "checked");

                // Add any attributes

                if (checkBoxHtmlAttributes != null)
                    checkboxList.MergeAttributes(checkBoxHtmlAttributes);

                checkboxList.SetInnerText(item.Text);
                output.Append(checkboxList.ToString(TagRenderMode.SelfClosing));
                output.Append("&nbsp; " + item.Text + "</label></div>");
            }
            return new MvcHtmlString(output.ToString());
        }

        #endregion

        #region public static MvcHtmlString File(this HtmlHelper html, string name)

        public static MvcHtmlString File(this HtmlHelper html, string name)
        {
            var tb = new TagBuilder("input");
            tb.Attributes.Add("type", "file");
            tb.Attributes.Add("name", name);
            tb.GenerateId(name);
            return MvcHtmlString.Create(tb.ToString(TagRenderMode.SelfClosing));
        }

        #endregion

        #region public static MvcHtmlString FileFor<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression)

        public static MvcHtmlString FileFor<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression)
        {
            string name = GetFullPropertyName(expression);
            return html.File(name);
        }

        #endregion

        #endregion

        #region private methods

        #region private static string GetRouteValuesString(UrlHelper urlHelper, object routeValues)

        /// <summary>
        /// Gets the route values string.
        /// </summary>
        /// <param name="urlHelper">The URL helper.</param>
        /// <param name="routeValues">The route values.</param>
        /// <returns></returns>
        private static string GetRouteValuesString(UrlHelper urlHelper, object routeValues)
        {
            var routeValueString = new StringBuilder();

            int paramCount = 0;

            foreach (PropertyDescriptor propertyDescriptor in TypeDescriptor.GetProperties(routeValues))
            {
                string name = propertyDescriptor.Name;
                object value = propertyDescriptor.GetValue(routeValues);

                if (value != null)
                {
                    if (paramCount > 0)
                        routeValueString.Append("&");
                    if (value.ToString().Contains("$") || value.ToString().Contains("#"))
                    {
                        routeValueString.AppendFormat("{0}=' + {1} + '", name, value);
                    }
                    else
                    {
                        routeValueString.Append(name + "=" + urlHelper.Encode(value.ToString()));
                    }
                }

                paramCount++;
            }

            return routeValueString.ToString();
        }

        #endregion

        #region private static MvcHtmlString BuildTable(string name, IEnumerable<Object> items, IDictionary<string, object> attributes)

        /// <summary>
        /// Builds the table.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="items">The items.</param>
        /// <param name="attributes">The attributes.</param>
        /// <returns></returns>
        private static MvcHtmlString BuildTable(string name, IEnumerable<Object> items,
                                                IDictionary<string, object> attributes)
        {
            var sb = new StringBuilder();
            BuildTableHeader(sb, items.FirstOrDefault().GetType());
            foreach (object item in items)
            {
                BuildTableRow(sb, item);
            }
            var builder = new TagBuilder("table");
            builder.MergeAttributes(attributes);
            builder.MergeAttribute("name", name);
            builder.InnerHtml = sb.ToString();
            return new MvcHtmlString(builder.ToString(TagRenderMode.Normal));
        }

        #endregion

        #region private static void BuildTableRow(StringBuilder sb, object obj)

        /// <summary>
        /// Builds the table row.
        /// </summary>
        /// <param name="sb">The sb.</param>
        /// <param name="obj">The obj.</param>
        private static void BuildTableRow(StringBuilder sb, object obj)
        {
            Type objType = obj.GetType();
            sb.AppendLine("\t<tr>");
            foreach (PropertyInfo property in objType.GetProperties())
            {
                sb.AppendFormat("\t\t<td>{0}</td>\n", property.GetValue(obj, null));
            }
            sb.AppendLine("\t</tr>");
        }

        #endregion

        #region private static void BuildTableHeader(StringBuilder sb, Type p)

        /// <summary>
        /// Builds the table header.
        /// </summary>
        /// <param name="sb">The sb.</param>
        /// <param name="p">The p.</param>
        private static void BuildTableHeader(StringBuilder sb, Type p)
        {
            sb.AppendLine("\t<tr>");
            foreach (PropertyInfo property in p.GetProperties())
            {
                sb.AppendFormat("\t\t<th>{0}</th>\n", property.Name);
            }
            sb.AppendLine("\t</tr>");
        }

        #endregion

        #region private static string GetFullPropertyName<T, TProperty>(Expression<Func<T, TProperty>> exp)

        private static string GetFullPropertyName<T, TProperty>(Expression<Func<T, TProperty>> exp)
        {
            MemberExpression memberExp;

            if (!TryFindMemberExpression(exp.Body, out memberExp))
                return string.Empty;

            var memberNames = new Stack<string>();

            do
            {
                memberNames.Push(memberExp.Member.Name);
            } while (TryFindMemberExpression(memberExp.Expression, out memberExp));

            return string.Join(".", memberNames.ToArray());
        }

        #endregion

        #region private static bool TryFindMemberExpression(Expression exp, out MemberExpression memberExp)

        private static bool TryFindMemberExpression(Expression exp, out MemberExpression memberExp)
        {
            memberExp = exp as MemberExpression;

            if (memberExp != null)
                return true;

            if (IsConversion(exp) && exp is UnaryExpression)
            {
                memberExp = ((UnaryExpression) exp).Operand as MemberExpression;

                if (memberExp != null)
                    return true;
            }

            return false;
        }

        #endregion

        #region private static bool IsConversion(Expression exp)

        private static bool IsConversion(Expression exp)
        {
            return (exp.NodeType == ExpressionType.Convert || exp.NodeType == ExpressionType.ConvertChecked);
        }

        #endregion

        #endregion

        #region Helper methods to return generated ID and Name of the MVC controls

        public static string NameFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
                                                        Expression<Func<TModel, TProperty>> expression)
        {
            return htmlHelper != null
                       ? htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(
                           ExpressionHelper.GetExpressionText(expression))
                       : string.Empty;
        }

        //public static string IdFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
        //                                              Expression<Func<TModel, TProperty>> expression)
        //{
        //    return HtmlHelper.GenerateIdFromName(NameFor(htmlHelper, expression));
        //}


        public static MvcHtmlString LabelFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, object htmlAttributes)
        {
            return LabelHelper(html, ModelMetadata.FromLambdaExpression(expression, html.ViewData), ExpressionHelper.GetExpressionText(expression), htmlAttributes);
        }

        private static MvcHtmlString LabelHelper(HtmlHelper html, ModelMetadata metadata, string htmlFieldName, object htmlAttributes)
        {
            var resolvedLabelText = metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();

            if (string.IsNullOrEmpty(resolvedLabelText))
            {
                return MvcHtmlString.Empty;
            }

            var tag = new TagBuilder("label");
            tag.Attributes.Add("for", TagBuilder.CreateSanitizedId(html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(htmlFieldName)));
            tag.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            tag.SetInnerText(resolvedLabelText);

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString Label(this HtmlHelper helper, string target, string text, string id, object htmlAttributes)
        {
            var tag = new TagBuilder("label");
            tag.Attributes.Add("for", TagBuilder.CreateSanitizedId(target));
            tag.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            tag.SetInnerText(text);

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));

            //return new MvcHtmlString(string.Format("<label id='{0}' for='{1}'>{2}</label>", id, target, text));
        }

        #endregion
    }

    #endregion
}