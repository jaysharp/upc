#region Copyright

// =============================================================================================================================================
//  Copyright (c) 2012 UnitedGaming
//  ALL RIGHTS RESERVED.  
//
//  Unauthorized distribution, adaptation or use may be subject to civil and criminal penalties.
// =============================================================================================================================================

#endregion

#region Header

// Author				:  
// Created date			: 19/08/2013 04:51 PM
// Description			: SessionHelper
// History
// =========================================================================================================================
// Date			BugID		Description														Developer       Reviewed By
// =========================================================================================================================
//                                                                                          Ilango K
// =========================================================================================================================

#endregion

#region Using

using System.Collections;
using System.Web;
using UpcGenarator.BusinessObjects;
using UpcGenarator.Web.Helpers;

#endregion

namespace UpcGenarator.Web.Helpers
{
    /// <summary>
    /// Summary description for SessionHelper
    /// </summary>
    public static class SessionHelper
    {
        #region Private Variables

        private const string SessionLogoninfo = "Session_LogOnInfo";
        private const string SessionUserinfo = "Session_UserInfo";
        private const string SessionHierarchy = "Session_Hierarchy";
        private const string SessionRememberMe = "Session_RememberMe";
        //Private Const SessionRememberMe As String = "Session_RememberMe"

        #endregion

        #region public static void NewSession()

        public static void NewSession()
        {
            HttpContext context = HttpContext.Current;
            // ReSharper disable RedundantCheckBeforeAssignment
            if (context.Session[SessionLogoninfo] != null)
                // ReSharper restore RedundantCheckBeforeAssignment
                context.Session[SessionLogoninfo] = null;

            context.Session[SessionLogoninfo] = new User();
        }

        #endregion

        #region IsSessionValid

        public static bool IsSessionValid
        {
            get
            {
                if (HttpContext.Current.Session == null)
                    return false;

                HttpContext context = HttpContext.Current;
                return context.Session[SessionLogoninfo] != null && ((User)context.Session[SessionLogoninfo]).IsActive;
            }
        }

        #endregion

        #region public static BusinessObjectsInfo BusinessObjectsInfo

        public static BusinessObjectsInfo BusinessObjectsInfo
        {
            get
            {
                HttpContext context = HttpContext.Current;
                if (context.Session[SessionUserinfo] == null)
                    context.Session[SessionUserinfo] = new BusinessObjectsInfo();
                return (BusinessObjectsInfo) context.Session[SessionUserinfo];
            }
        }

        #endregion

        #region public static AdminUser SessionLogOnInfo

        /// <summary>
        /// Gets or sets the session log on info.
        /// </summary>
        /// <value>
        /// The session log on info.
        /// </value>
        public static User SessionLogOnInfo
        {
            get
            {
                HttpContext context = HttpContext.Current;
                if (context.Session[SessionLogoninfo] == null)
                    context.Session[SessionLogoninfo] = new User();

                return (User)context.Session[SessionLogoninfo];
            }
            set
            {
                HttpContext context = HttpContext.Current;
                context.Session[SessionLogoninfo] = value;
            }
        }

        #endregion

        #region public static void PushSession(AdminUser users)

        /// <summary>
        /// Pushes the session.
        /// </summary>
        /// <param name="users">The users.</param>
        public static void PushSession(User users)
        {
            HttpContext context = HttpContext.Current;


            if (context.Session[SessionHierarchy] != null)
            {
                ((Stack) context.Session[SessionHierarchy]).Push(users);
            }
            else
            {
                var st = new Stack();
                st.Push(users);
                context.Session[SessionHierarchy] = st;
            }
        }

        #endregion

        #region public static AdminUser PopSession()

        /// <summary>
        /// Pops the session.
        /// </summary>
        /// <returns></returns>
        public static User PopSession()
        {
            HttpContext context = HttpContext.Current;
            User logOnInfo = null;
            Stack st;

            if (context.Session[SessionHierarchy] != null)
            {
                st = (Stack) context.Session[SessionHierarchy];

                if (st.Count > 0)
                    logOnInfo = (User)st.Pop();
            }

            return logOnInfo;
        }

        #endregion

        #region public static void ClearSessionStack()

        /// <summary>
        /// Clears the session stack.
        /// </summary>
        public static void ClearSessionStack()
        {
            HttpContext context = HttpContext.Current;
            context.Session[SessionLogoninfo] = null;
            context.Session[SessionUserinfo] = null;
        }

        #endregion 

        public static bool RememberMe
        {
            get
            {
                HttpContext context = HttpContext.Current;
                if (context.Session[SessionRememberMe] == null)
                {
                    context.Session[SessionRememberMe] = false;
                }

                return (bool)context.Session[SessionRememberMe];
            }
            set
            {
                HttpContext context = HttpContext.Current;
                context.Session[SessionRememberMe] = value;
            }
        }

    }
}