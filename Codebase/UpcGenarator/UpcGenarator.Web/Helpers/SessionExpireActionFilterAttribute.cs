﻿#region Copyright
// =============================================================================================================================================
//   
//  ALL RIGHTS RESERVED. 
//
//  Unauthorized distribution, adaptation or use may be subject to civil and criminal penalties.
// ============================================================================================================================================
#endregion

#region Header
// Copyrights			: 
// Author				: Raja
// Created date			: Aug 09, 2013
// Description			:  
// History
// =========================================================================================================================
// Date			BugID		Description														Developer       Reviewed By
// =========================================================================================================================
// 08042013                Created                                                         Raja
// =========================================================================================================================
#endregion

#region using

using System;
using System.Globalization;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

#endregion

namespace UpcGenarator.Web.Helpers
{
    /// <summary>
    /// Helps to avoid accesing the pages with out loging in(if user does not log in, then it will redirect to login page.)
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public sealed class SessionExpireActionFilterAttribute : ActionFilterAttribute
    {
        #region public override void OnActionExecuting(ActionExecutingContext filterContext)

        /// <summary>
        /// Called by the ASP.NET MVC framework before the action method executes.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (filterContext == null) throw new ArgumentNullException("filterContext");

            IFormatProvider provider = CultureInfo.CurrentCulture;

            if (filterContext.HttpContext != null)
            {
                HttpContextBase ctx = filterContext.HttpContext;
                RouteData rt = filterContext.RouteData;

                if (ctx.Request.Url != null)
                {
                    string redirectTo = string.Format(provider, "~/Account/LogOn?ReturnUrl={0}",
                                                      HttpUtility.UrlEncode(ctx.Request.RawUrl));

                    if (ctx.Session != null)
                    {
                        if (ctx.Session.IsNewSession)
                        {
                            filterContext.Result = ctx.Request.IsAjaxRequest()
                                                       ? (ActionResult)
                                                         new JsonResult {Data = new {LogonRequired = true}}
                                                       : new RedirectResult(redirectTo);
                            return;
                        }

                        //if (!SessionHelper.IsSessionValid)
                        //{
                        //    if ((!rt.Values["Controller"].ToString().Equals("Account") &&
                        //         !rt.Values["Action"].ToString().Equals("LogOn")) ||
                        //        (rt.Values["Controller"].ToString().Equals("Account") &&
                        //         !rt.Values["Action"].ToString().Equals("LogOn")))
                        //    {
                        //        filterContext.Result = ctx.Request.IsAjaxRequest()
                        //                                   ? (ActionResult)
                        //                                     new JsonResult {Data = new {LogonRequired = true}}
                        //                                   : new RedirectResult(redirectTo);
                        //    }

                        //    return;
                        //}
                    }
                }
            }
        }

        #endregion
    }
}