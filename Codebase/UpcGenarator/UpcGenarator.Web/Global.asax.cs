﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace UpcGenarator.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {

        public static string EncryptionKey { get; set; }
        public static bool UseAdAuthendication { get; set; }


        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
        }

        private static void LoadAppSettings()
        {
            UseAdAuthendication = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["UseADAuthendication"])&& Convert.ToBoolean(ConfigurationManager.AppSettings["UseADAuthendication"]);
        }
        #region protected void Session_Start()

        /// <summary>
        /// Session_s the start.
        /// </summary>
        protected void Session_Start()
        {
            LoadAppSettings();
        }

        #endregion  



        public static CultureInfo Culture { get; set; }
    }
}