﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using UpcGenarator.BusinessAccess;
using UpcGenarator.Web.Helpers;
using UpcGenarator.Web.Models;


namespace UpcGenarator.Web.Controllers
{
    public class HomeController : Controller
    {
        #region Global Variable

        readonly UpcGenarateBusinessAccess _oUpcGenarateBusinessAccess = new UpcGenarateBusinessAccess();
        #endregion

        #region  public ActionResult UpcAdd(int Id)
        [HttpGet]
        public ActionResult UpcAdd(Int32? id)
        {
            var oUpcGenarateModel = new UpcGenarateModel {Statuslist = GetStatus("0", true)};

            if (id > 0)
            {
                var edit = _oUpcGenarateBusinessAccess.UpcGenaratorSelectById(id);
                oUpcGenarateModel.Id = edit[0].Id;
                oUpcGenarateModel.StatusId = edit[0].StatusId;
                oUpcGenarateModel.ItemNumber = edit[0].ItemNumber;
                oUpcGenarateModel.Description = edit[0].Description;
                oUpcGenarateModel.SingleUpc =  edit[0].SingleUpc;
                oUpcGenarateModel.MasterUpc = edit[0].MasterUpc;
                oUpcGenarateModel.Qty = edit[0].Qty;
                oUpcGenarateModel.InnerUpc = edit[0].InnerUpc;
                oUpcGenarateModel.Innerqty = edit[0].InnerQty;
                
            }


            return View(oUpcGenarateModel);
        }
        #endregion

        #region  public ActionResult UpcAdd(UpcGenarateModel oUpcGenarateModel)
        [HttpPost]
        public ActionResult UpcAdd(UpcGenarateModel oUpcGenarateModel)
        {

            if (oUpcGenarateModel.Id == 0)
            {
                _oUpcGenarateBusinessAccess.Insert(oUpcGenarateModel);
            }
            else
            {
                _oUpcGenarateBusinessAccess.Update(oUpcGenarateModel);
            }
           

            return RedirectToAction(Actions.Home_Index);
        }

        #endregion

        #region  public ActionResult Index()
        public ActionResult Index()
        {
            try
            {
                var upcmodel = new List<UpcGenarateModel>();
                var result = _oUpcGenarateBusinessAccess.UpcSelectAll();
                upcmodel.AddRange(result.Select(u => new UpcGenarateModel
                {
                    Id = u.Id,
                    ItemNumber = u.ItemNumber,
                    Description = u.Description,
                    SingleUpc = u.SingleUpc,
                    MasterUpc = u.MasterUpc,
                    InnerUpc = u.InnerUpc,
                    Qty = u.Qty,
                    Innerqty = u.InnerQty,
                    StatusName = u.StatusName
                }).ToList());

               var oUpcGenarateModel = new UpcGenarateModel {UpcList = upcmodel};
                return View(oUpcGenarateModel);
                
            }
            catch (Exception)
            {
                //logger.Error("Error:" + ex);
                return null;
            }
        }
            #endregion

        #region  private SelectList GetStatus(string selected, bool addDefault)

         /// <summary>
        /// Gets the GetStatus
        /// </summary>
        /// <param name="selected">The selected.</param>
        /// <param name="addDefault">if set to <c>true</c> [add default].</param>
        /// <returns></returns>
        private  SelectList GetStatus(string selected, bool addDefault)
        {


            Dictionary<string, string> statuslist = _oUpcGenarateBusinessAccess.StatusSelectAll().ToDictionary
                (
                    status => status.Id.ToString(MvcApplication.Culture),
                    status => status.Name
                );

            if (addDefault)
                statuslist.Add("0", "Select Status");

            var result = new SelectList(
                from pair in statuslist orderby pair.Key ascending select pair,
                "Key",
                "Value",
                selected
                );

            return result;
        }

        #endregion

        #region public ActionResult GetSingleUpcmax()
        /// <summary>
        /// Description: To get the value for  SingleUpcmax VALUE
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetSingleUpcmax()
        {
            try
            {


                var result = _oUpcGenarateBusinessAccess.SelectSingleUpcmaxvalue();

                return new JsonResult { Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            catch (Exception)
            {
              
                return Json(new { Error = "Error" });
            }
        }

        #endregion

    }
}
