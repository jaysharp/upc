﻿
#region Copyright

// =============================================================================================================================================
//  Copyright (c) 2012 UnitedGaming
//  ALL RIGHTS RESERVED.  
//
//  Unauthorized distribution, adaptation or use may be subject to civil and criminal penalties.
// =============================================================================================================================================

#endregion

#region Header

// Author				:  
// Created date			: 11/05/2015 12:46 PM
// Description			: Base Controller
// History
// =========================================================================================================================
// Date			BugID		Description														Developer       Reviewed By
// =========================================================================================================================
//                                                                                          G Raja
// =========================================================================================================================

#endregion

#region Using
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UpcGenarator.Web.Helpers;
#endregion

namespace UpcGenarator.Web.Controllers
{
    public class BaseController : Controller
    {
        #region Properties

        #region  public string SuccessMessage
        /// <summary>
        /// Property to get and set the success message
        /// </summary>
        public string SuccessMessage
        {
            get { return TempData[CommonHelper.SuccessMessageKey] as string; }
            set
            {
                if (TempData.ContainsKey(CommonHelper.SuccessMessageKey))
                    TempData[CommonHelper.SuccessMessageKey] = value;
                else
                    TempData.Add(CommonHelper.SuccessMessageKey, value);

                TempData.Remove(CommonHelper.ErrorMessageKey);
            }
        }
        #endregion

        #region  public string ErrorMessage
        /// <summary>
        /// Property to get and set the error message
        /// </summary>
        public string ErrorMessage
        {
            get { return TempData[CommonHelper.ErrorMessageKey] as string; }
            set
            {
                if (TempData.ContainsKey(CommonHelper.ErrorMessageKey))
                    TempData[CommonHelper.ErrorMessageKey] = value;
                else
                    TempData.Add(CommonHelper.ErrorMessageKey, value);

                TempData.Remove(CommonHelper.SuccessMessageKey);
            }
        }
       #endregion

        #region public bool CanClearMessage
        /// <summary>
        /// Property to get and set if the error message can be cleared
        /// </summary>
        public bool CanClearMessage
        {
            get
            {
                object objClearMessage = TempData[CommonHelper.CanClearMessageKey];
                return objClearMessage == null || Convert.ToBoolean(objClearMessage, CultureInfo.CurrentCulture);
            }
            set
            {
                if (TempData.ContainsKey(CommonHelper.CanClearMessageKey))
                    TempData[CommonHelper.CanClearMessageKey] = value;
                else
                    TempData.Add(CommonHelper.CanClearMessageKey, value);
            }
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Method to clear both success and error message
        /// </summary>
        public void ClearMessage()
        {
            TempData.Remove(CommonHelper.SuccessMessageKey);
            TempData.Remove(CommonHelper.ErrorMessageKey);
        }

        #endregion
    }
}
