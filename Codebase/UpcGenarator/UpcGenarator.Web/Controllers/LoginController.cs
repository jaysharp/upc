﻿using System;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.Web.Mvc;
using UpcGenarator.Web.Helpers;
using UpcGenarator.Web.Models;
using UpcGenarator.Common;

namespace UpcGenarator.Web.Controllers
{
    public class LoginController :BaseController
    {

        #region Private Properties
        private static string[] AdConnectionComponents
        {
            get { return ConfigurationManager.ConnectionStrings["ADConnectionString"].ConnectionString.Split('/'); }
        }

        private static PrincipalContext AdPrincipal
        {
            get
            {
                string domain = AdConnectionComponents[2];
                string container = AdConnectionComponents[3];
                return new PrincipalContext(ContextType.Domain, domain, container);
            }
        }

        private Boolean ValidateUser(string username, string password)
        {
            const bool result = false;


	        try {

		        if (MvcApplication.UseAdAuthendication)
		        {
		            if (!AdPrincipal.ValidateCredentials(username, password)) {
				        throw new Exception("Unable to authendicate the user on Active Directory, Please enter valid UserName/Password.");
			        }
		            return true;
		        }


                //if (result)
                //{//   
                //    CommonHelper.SetCookieInfo(SessionHelper.RememberMe, username, password);
                //    FormsAuthentication.RedirectFromLoginPage(username, SessionHelper.RememberMe);
                //    FormsAuthentication.SetAuthCookie(username, SessionHelper.RememberMe);
                //}


	} catch (Exception ex) {
        return false;
	}

	return result;
}

        #endregion


        #region  public ActionResult Login()

        public ActionResult Login()
        {
            return View();
        }

        #endregion

        #region  public ActionResult Login(UserModel oUserModel)

        [HttpPost]
        public ActionResult Login(UserModel oUserModel)
        {

            ClearMessage();

            if (oUserModel == null)
            {
                throw new ArgumentNullException("oUserModel");
            }


            if (ModelState.IsValid)
            {
                var result = ValidateUser(oUserModel.UserName, oUserModel.Password);

                if ((MvcApplication.UseAdAuthendication && result)||(!MvcApplication.UseAdAuthendication))
                {
                    return RedirectToAction(Actions.Home_Index, Helpers.Controllers.Home);
                    
                }
                else
                {
                    ErrorMessage = Convert.ToString(UserMessages.LOGIN_USERNAME_PASSWORD_INVALID);
                }
               
            }

            return View();

        }

        #endregion
          
    }
}
