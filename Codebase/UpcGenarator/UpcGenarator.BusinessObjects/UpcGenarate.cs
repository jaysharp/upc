﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpcGenarator.BusinessObjects
{
    public  class UpcGenarate
    {
        #region Properties

        public virtual int Id { get; set; }

        public virtual int StatusId { get; set; }

        public virtual string ItemNumber { get; set; }

        public virtual string Description { get; set; }

        public virtual string SingleUpc { get; set; }

        public virtual string MasterUpc { get; set; }

        public virtual string InnerUpc { get; set; }

        public virtual string Qty { get; set; }

        public virtual string Innerqty { get; set; }

        public virtual int CreatedBy { get; set; }

        public virtual DateTime CreateDate { get; set; }

        #endregion
    }
}
