﻿using System;

namespace UpcGenarator.BusinessObjects
{
    public class User
    {
        #region Properties

        public virtual string UserName { get; set; }

        public virtual string Password { get; set; }

        public virtual Boolean IsActive { get; set; }

        public virtual Boolean RememberMe { get; set; }

        #endregion

    }
}
