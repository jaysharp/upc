﻿using System;
using System.Collections.Generic;
using System.Linq;
using UpcGenarator.BusinessObjects;
using UpcGenarator.DataAccess;
using System.Data.Objects;

 

namespace UpcGenarator.BusinessAccess
{
    public class UpcGenarateBusinessAccess
    {
        #region Global variables
        UPCEntities upcEntities = new UPCEntities(); 
        #endregion

        #region  public Int32 Insert(UpcGenarate oUpcGenarate)
        /// <summary>
        /// Description : Business Method for UpcGenarate insert
        /// </summary>
        /// <param name="oUpcGenarate"></param>
        /// <returns></returns>

        public Int32 Insert(UpcGenarate oUpcGenarate)
        {
            try
            {

                var idParameter = new ObjectParameter("Id", typeof(int));
                var result = upcEntities.USP_UpcGenarator_Insert(oUpcGenarate.StatusId, oUpcGenarate.ItemNumber, oUpcGenarate.Description, oUpcGenarate.SingleUpc, oUpcGenarate.MasterUpc,oUpcGenarate.InnerUpc,oUpcGenarate.Qty,oUpcGenarate.Innerqty,idParameter);
                return Convert.ToInt32(idParameter.Value);
            }
            catch (Exception ex)
            { 
                return 0;
            }

        }

        #endregion

        #region   public List<USP_Status_SelectAll_Result> StatusSelectAll()
        /// <summary>
        /// Description: Business Method for GetStatus
        /// </summary>
        /// <returns></returns>
        public List<USP_Status_SelectAll_Result> StatusSelectAll()
        {
            try
            {

                var res = upcEntities.USP_Status_SelectAll();
                return res.ToList<USP_Status_SelectAll_Result>();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion

        #region   public List<USP_UpcGenarator_SelectSingelUpcMax_Result> SelectSingleUpcmaxvalue()
        /// <summary>
        /// Description: Business Method for SelectSingleUpcmaxvalue
        /// </summary>
        /// <returns></returns>
        public List<USP_UpcGenarator_SelectSingelUpcMax_Result> SelectSingleUpcmaxvalue()
        {
            try
            {

                var res = upcEntities.USP_UpcGenarator_SelectSingelUpcMax();
                return res.ToList<USP_UpcGenarator_SelectSingelUpcMax_Result>();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion

        #region   public List<USP_UpcGenarator_SelectAll_Result> UpcSelectAll()
        /// <summary>
        /// Description: Business Method for UPC SelectAll
        /// </summary>
        /// <returns></returns>
        public List<USP_UpcGenarator_SelectAll_Result> UpcSelectAll()
        {
            try
            {
                var res = upcEntities.USP_UpcGenarator_SelectAll();
                return res.ToList<USP_UpcGenarator_SelectAll_Result>();
            }
            catch (Exception ex)
            {
                //logger.Error("Error:" + ex);
                return null;
            }
        }

        #endregion    

        #region   public Int32 Update(UpcGenarate oUpcGenarate)
        /// <summary>
        /// Description : Business Method for UpcGenarate Update
        /// </summary>
        /// <param name="oUpcGenarate"></param>
        /// <returns></returns>

        public Int32 Update(UpcGenarate oUpcGenarate)
        {
            try
            {
                var result = upcEntities.USP_UpcGenarator_Update(oUpcGenarate.Id,oUpcGenarate.StatusId, oUpcGenarate.ItemNumber, oUpcGenarate.Description, oUpcGenarate.SingleUpc, oUpcGenarate.MasterUpc, oUpcGenarate.InnerUpc, oUpcGenarate.Qty, oUpcGenarate.Innerqty);
                return result;
            }
            catch (Exception ex)
            {
                //logger.Error("Error:" + ex);
                return 0;
            }
        }

        #endregion

        #region   public List<USP_UpcGenarator_SelectById_Result> UpcGenaratorSelectById(Int32 Id)
        /// <summary>
        /// Description: Business Method for UpcGenarator SelectBy Id
        /// </summary>
        /// <returns></returns>
        public List<USP_UpcGenarator_SelectById_Result> UpcGenaratorSelectById(Int32? Id)
        {
            try
            {
                var res = upcEntities.USP_UpcGenarator_SelectById(Id);
                return res.ToList<USP_UpcGenarator_SelectById_Result>();
            }
            catch (Exception ex)
            {
               // logger.Error("Error:" + ex);
                return null;
            }
        }

        #endregion
    }
}
